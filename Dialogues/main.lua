local winny
local winny_id

local function hideWinny()
  winny:destroy()
  if tes3ui.menuMode then tes3ui.leaveMenuMode() end
end

local function showWinny()
  if tes3ui.findMenu(winny_id) then return end
  winny_id = tes3ui.registerID('winny')
  winny = tes3ui.createMenu
  {
   id = winny_id,
   fixedFrame = true,
  }
  --winny.alpha = 1.0
  winny:createLabel
  {
   text = 'winny',
  }
  local button = winny:createButton
  {
   id = tes3ui.registerID('Cancel'),
   text = '',
  }
  button:register("mouseClick", hideWinny)
  winny:register("keyEnter", hideWinny)
  winny:updateLayout()
  tes3ui.enterMenuMode(winny)
end
event.register("keyDown", showWinny, {filter = 44}) -- z



