
local this = {}

-- basic window fabric method
this.new = function(name)

local m = {                    -- a new window
 id = tes3ui.registerID(name),
 ltext = ' Enter: ',
 block = {[1] = ''},
}
m.menu = tes3ui.createMenu{id = m.id, fixedFrame = true}
m.menu.alpha = 1.0

m.show = function()
  m.menu:updateLayout()
  tes3ui.enterMenuMode(m.menu)
end

m.hide = function()
  if not tes3ui.findMenu(m.id) then return end
  m.menu:destroy()
  if tes3ui.menuMode then tes3ui.leaveMenuMode() end
end

m.addBlock = function(n, width, ltext)
  m.block[n] = m.menu:createBlock{}
  m.block[n].width = width or 360
  m.block[n].autoHeight = true
  m.block[n].childAlignX = 0.5
  m.block[n].childAlignY = 0.5
  m.block[n].flowDirection = 'top_to_bottom'
  
  if ltext then
    m.block[n]:createLabel
    {
     text = ltext,
     childAlignX = 0.5,
    }
  end
end

m.addBlockButton = function(n, buttext, func)
  local button = m.block[n]:createButton
  {
    id = tes3ui.registerID(buttext),
    text = buttext,
  }
  button:register("mouseClick", func)
end

  return m
end

return this
